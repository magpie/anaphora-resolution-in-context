module ModelE where 

import Data.List
import Model hiding (give)

stick    = list2OnePlacePred [U]
stone    = list2OnePlacePred [Q]
flower   = list2OnePlacePred [H]

like  = curry (`elem` [(x,y) | x <- entities, person x,
															 y <- entities, female y])
hug 	=  curry (`elem` [(x,y) | x <- entities, person x,
																y <- entities, person y])

strike :: ThreePlacePred
strike = curry3 (`elem` [(x,y,z) | x <- entities, 
                                y <- entities,
																z <- entities,
                                giant x && (stone y || sword y || stick y) &&
																(stone z || sword z || stick z)
																])
																
give :: ThreePlacePred
give = curry3 (`elem` [(x,y,z) | x <- entities, person x,
                                y <- entities, person y,
																z <- entities, thing z])
																